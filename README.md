# TypeScript 3.0: A Practical Guide

Contains code and notes from studying [TypeScript 3.0: A Practical Guide](https://www.udemy.com/typescriptguide/learn/v4/overview)

## Code folders

The code folders contains sub folders - one per section of the course.

Each of these section folder is a [ParcelJs](https://parceljs.org/) project, where:

- src: contains source
- dist: contains compiled code

to execute the code from the section sub folder, run: `npm run develop`. Example:

```bash
cd code/basics
npm run develop
...
> basics@1.0.0 develop /home/atanas/Projects/Atanas/typescriptguide/code/basics
> parcel src/index.html

Server running at http://localhost:1234
✨  Built in 1.82s.
```
