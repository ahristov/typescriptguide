"use strict";
exports.__esModule = true;
var h = require("./helpers");
h.clear();
/* -------------------------------------------------------------------- */
h.addH2('Implicit type assignment');
var isReady1 = true; // boolean
var age1 = 24; // number
var myName1 = 'Atanas'; // string
var names1 = ['Atanas', 'Alex']; // array of strings
var address1 = [123, 'Sunny Dr']; // array of any
// names1 = [1, 2, 3]; // Error: Type 'number' is not assignable to type 'string'
address1 = ['Sunny Dr', 123]; // This works
h.addCode("\n\nlet isReady1 = true; // boolean\nlet age1 = 24; // number\nlet myName1 = 'Atanas'; // string\nlet names1 = ['Atanas', 'Alex']; // array of strings\nlet address1 = [123, 'Sunny Dr']; // array of any\n\n// names1 = [1, 2, 3]; // Error: Type 'number' is not assignable to type 'string'\naddress1 = ['Sunny Dr', 123]; // This works\n\n");
/* -------------------------------------------------------------------- */
h.addH2('Explicit type assignment');
var isReady2 = true; // boolean
var age2 = 24; // number
var myName2 = 'Atanas'; // string
var names2 = ['Atanas', 'Alex']; // array of strings
var address2 = [123, 'Sunny Dr']; // array of any
h.addCode("\n\nlet isReady2: boolean = true; // boolean\nlet age2: number = 24; // number\nlet myName2: string = 'Atanas'; // string\nlet names2: string[] = ['Atanas', 'Alex']; // array of strings\nlet address2: any[] = [123, 'Sunny Dr']; // array of any\n\n");
/* -------------------------------------------------------------------- */
h.addH2('Tuples');
h.addRemark('<b>Tuple:</b> an array where the type of a fixed number of elements is known');
var address3 = [123, 'Street'];
// address3 = ['Street', 123]; // Error: string is not assignable to number
// address3 = [123, 'Street', 123]; // Error: [number, string, number] is not assignable to [number, string]
h.addCode("\n\nlet address3: [number, string] = [123, 'Street'];\n// address3 = ['Street', 123]; // Error: string is not assignable to number\n// address3 = [123, 'Street', 123]; // Error: [number, string, number] is not assignable to [number, string]\n\n");
/* -------------------------------------------------------------------- */
h.addH2('Returns and Parameters');
// function returnName(name):string {} // Error: A function whose declared type is neither 'void' or 'any' must return a value.
function returnName(name) {
    return name;
}
// function greet():void { return 1; } // Error: type 1 is not assignable  to type void.
function greet() {
    console.log('Hello');
}
function multiply1(a, b) {
    return a * b;
}
console.log(multiply1(2, 4)); // Prints 8
console.log(multiply1(2, 'abc')); // Prints NaN
function multiply2(a, b) {
    return a * b;
}
console.log(multiply2(2, 4)); // Prints 8
// console.log(multiply2(2, 'abc')); // Error: Argument of type '"abd"' is not assignable to parameter of type 'number'.
h.addCode("\n\n// function returnName(name):string {} // Error: A function whose declared type is neither 'void' or 'any' must return a value.\nfunction returnName(name):string {\n  return name;\n}\n\n// function greet():void { return 1; } // Error: type 1 is not assignable  to type void.\nfunction greet():void { \n  console.log('Hello'); \n}\n\nfunction multiply1(a, b): number {\n  return a * b;\n}\n\nconsole.log(multiply1(2, 4)); // Prints 8\nconsole.log(multiply1(2, 'abc')); // Prints NaN\n\nfunction multiply2(a:number, b:number): number {\n  return a * b;\n}\n\nconsole.log(multiply2(2, 4)); // Prints 8\n// console.log(multiply2(2, 'abc')); // Error: Argument of type '\"abd\"' is not assignable to parameter of type 'number'.\n\n");
/* -------------------------------------------------------------------- */
h.addH2('Type Never');
h.addRemark("\n<b>Type Never:</b> Represents the type of values that never occur.<br/>\nFor example, a function that always throws exception or one that never returns.<br/>\nOr variable, narrowed by type guards that can never be true.<br/>\n");
// function monkeyWrench1() {
//   while(true) {
//     console.log('I will break your browser one day');
//   }
//   return true; // Error: Unreachable code detected.
// }
function monkeyWrench2() {
    while (true) {
        console.log('I will break your browser one day');
    }
}
monkeyWrench2();
h.addCode("\n\n");
