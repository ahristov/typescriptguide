import * as h from './helpers';

h.clear();

/* -------------------------------------------------------------------- */

h.addH2('Primitive/basic types');

let flag; // of type any

h.addCode(`
// string: Represents a sequence of Unicode characters
// number: Represents both integers and floating-point numbers
// boolean: Represents a logical true or false
// undefined: Represents a value that hasn't been initialized yet
// null: Represents no value
// any: Opting out of type checking. Can be used for dynamic content or values from third-party libraries for example
// void: Used to represent a non-returning function
// never: Used to specify unreachable areas of code

let flag; // of type any
`);

/* -------------------------------------------------------------------- */

h.addH2('Type inference: Implicit type assignment');

let isReady1 = true; // boolean
let age1 = 24; // number
let myName1 = 'Atanas'; // string
let names1 = ['Atanas', 'Alex']; // array of strings
let address1 = [123, 'Sunny Dr']; // array of any

// names1 = [1, 2, 3]; // Error: Type 'number' is not assignable to type 'string'
address1 = ['Sunny Dr', 123]; // This works

h.addCode(`
let isReady1 = true; // boolean
let age1 = 24; // number
let myName1 = 'Atanas'; // string
let names1 = ['Atanas', 'Alex']; // array of strings
let address1 = [123, 'Sunny Dr']; // array of any

// names1 = [1, 2, 3]; // Error: Type 'number' is not assignable to type 'string'
address1 = ['Sunny Dr', 123]; // This works

`);

/* -------------------------------------------------------------------- */

h.addH2('Type annotations: Explicit type assignment');

let isReady2: boolean = true; // boolean
let age2: number = 24; // number
let myName2: string = 'Atanas'; // string
let names2: string[] = ['Atanas', 'Alex']; // array of strings
let address2: any[] = [123, 'Sunny Dr']; // array of any

h.addCode(`
let isReady2: boolean = true; // boolean
let age2: number = 24; // number
let myName2: string = 'Atanas'; // string
let names2: string[] = ['Atanas', 'Alex']; // array of strings
let address2: any[] = [123, 'Sunny Dr']; // array of any

`);

/* -------------------------------------------------------------------- */

h.addH2('Tuples');
h.addRemark('<b>Tuple:</b> an array where the type of a fixed number of elements is known');

let address3: [number, string] = [123, 'Street'];
// address3 = ['Street', 123]; // Error: string is not assignable to number
// address3 = [123, 'Street', 123]; // Error: [number, string, number] is not assignable to [number, string]

h.addCode(`
let address3: [number, string] = [123, 'Street'];
// address3 = ['Street', 123]; // Error: string is not assignable to number
// address3 = [123, 'Street', 123]; // Error: [number, string, number] is not assignable to [number, string]

`);

/* -------------------------------------------------------------------- */

h.addH2('Returns and Parameters');

// function returnName(name):string {} // Error: A function whose declared type is neither 'void' or 'any' must return a value.
function returnName(name):string {
  return name;
}

// function greet():void { return 1; } // Error: type 1 is not assignable  to type void.
function greet():void { 
  console.log('Hello'); 
}

function multiply1(a, b): number {
  return a * b;
}

console.log(multiply1(2, 4)); // Prints 8
console.log(multiply1(2, 'abc')); // Prints NaN

function multiply2(a:number, b:number): number {
  return a * b;
}

console.log(multiply2(2, 4)); // Prints 8
// console.log(multiply2(2, 'abc')); // Error: Argument of type '"abd"' is not assignable to parameter of type 'number'.


h.addCode(`
// function returnName(name):string {} // Error: A function whose declared type is neither 'void' or 'any' must return a value.
function returnName(name):string {
  return name;
}

// function greet():void { return 1; } // Error: type 1 is not assignable  to type void.
function greet():void { 
  console.log('Hello'); 
}

function multiply1(a, b): number {
  return a * b;
}

console.log(multiply1(2, 4)); // Prints 8
console.log(multiply1(2, 'abc')); // Prints NaN

function multiply2(a:number, b:number): number {
  return a * b;
}

console.log(multiply2(2, 4)); // Prints 8
// console.log(multiply2(2, 'abc')); // Error: Argument of type '"abd"' is not assignable to parameter of type 'number'.

`);

/* -------------------------------------------------------------------- */

h.addH2('Type Never');
h.addRemark(`
<b>Type Never:</b> Represents the type of values that never occur.<br/>
For example, a function that always throws exception or one that never returns.<br/>
Or variable, narrowed by type guards that can never be true.<br/>
`);

// function monkeyWrench1() {
//   while(true) {
//     console.log('I will break your browser one day');
//   }
//   return true; // Error: Unreachable code detected.
// }

// function monkeyWrench2(): never {
//   while(true) {
//     console.log('I will break your browser one day');
//   }
// }
// monkeyWrench2();

h.addCode(`

function monkeyWrench1() {
  while(true) {
    console.log('I will break your browser one day');
  }
  return true; // Error: Unreachable code detected.
}

`);


h.addH2('Enumerations');

enum OrderStatus {
  Paid, // starting value is 0 (zero)
  Shipped, 
  Completed, 
  Cancelled 
}
enum OrderStatus2 {
  Paid = 1, // starting value can be set
  Shipped, 
  Completed, 
  Cancelled 
}
enum OrderStatus3 {
  Paid = 1,
  Shipped = 2, 
  Completed = 13, 
  Cancelled // will be 14 - next unset value is incremental
}
// all values can be explicitly set
enum OrderStatus4 {
  Paid = 1,
  Shipped = 2, 
  Completed = 3, 
  Cancelled =0
}

let orderStatus = OrderStatus.Paid;
console.log('orderStatus:', orderStatus); // "orderStatus:" 0
console.log('OrderStatus.Paid:', OrderStatus.Paid); // "OrderStatus.Paid:" 0
console.log('OrderStatus[0]:', OrderStatus[0]); // "OrderStatus[0]:" "Paid"

/*

// JavaScript:
var OrderStatus;
(function (OrderStatus) {
    OrderStatus[OrderStatus["Paid"] = 0] = "Paid";
    OrderStatus[OrderStatus["Shipped"] = 1] = "Shipped";
    OrderStatus[OrderStatus["Completed"] = 2] = "Completed";
    OrderStatus[OrderStatus["Cancelled"] = 3] = "Cancelled";
})(OrderStatus || (OrderStatus = {}));

*/

h.addCode(`

enum OrderStatus {
  Paid, // starting value is 0 (zero)
  Shipped, 
  Completed, 
  Cancelled 
}
enum OrderStatus2 {
  Paid = 1, // starting value can be set
  Shipped, 
  Completed, 
  Cancelled 
}
enum OrderStatus3 {
  Paid = 1,
  Shipped = 2, 
  Completed = 13, 
  Cancelled // will be 14 - next unset value is incremental
}
// all values can be explicitly set
enum OrderStatus4 {
  Paid = 1,
  Shipped = 2, 
  Completed = 3, 
  Cancelled =0
}

let orderStatus = OrderStatus.Paid;
console.log('orderStatus:', orderStatus); // "orderStatus:" 0
console.log('OrderStatus.Paid:', OrderStatus.Paid); // "OrderStatus.Paid:" 0
console.log('OrderStatus[0]:', OrderStatus[0]); // "OrderStatus[0]:" "Paid"

/*

// JavaScript:
var OrderStatus;
(function (OrderStatus) {
    OrderStatus[OrderStatus["Paid"] = 0] = "Paid";
    OrderStatus[OrderStatus["Shipped"] = 1] = "Shipped";
    OrderStatus[OrderStatus["Completed"] = 2] = "Completed";
    OrderStatus[OrderStatus["Cancelled"] = 3] = "Cancelled";
})(OrderStatus || (OrderStatus = {}));

*/

`);


h.addH2('Objects');
h.addRemark('The <b>object type</b> is shared with JavaScript and represents a non-primitive type.')
const customer = {
  name: 'Atanas Hristov Ltd', // string
  turnover: 2000134, // number
  active: true, // boolean
  profit: null // any
}

customer.turnover = 500000;
customer.profit = '';
customer.profit = 123; 
console.log(customer.turnover);

h.addCode(`
const customer = {
  name: 'Atanas Hristov Ltd', // string
  turnover: 2000134, // number
  active: true, // boolean
  profit: null // any
}

customer.turnover = 500000;
customer.profit = '';
customer.profit = 123; 
console.log(customer.turnover);
`);


h.addH2('Arrays');
h.addRemark('<b>Arrays</b> are structures that TypeScript inherits from JavaScript.');

const numbers: number[] = []; // we can add type annotation
numbers.push(1);
// numbers.push('two'); // 'two' is not assignable to parameter of type number

for (let i in numbers) {
  console.log(numbers[i]); 
}
numbers.forEach(function (num) { // num is a number
  console.log(num);
});


h.addCode(`
const numbers: number[] = []; // we can add type annotation
numbers.push(1);
// numbers.push('two'); // 'two' is not assignable to parameter of type number

for (let i in numbers) {
  console.log(numbers[i]); 
}
numbers.forEach(function (num) { // num is a number
  console.log(num);
});

`);

