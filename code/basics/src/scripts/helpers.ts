import * as Prism from 'prismjs';

export function getRoot():HTMLElement {
  return document.getElementById('root');
}

export function clear(root?: HTMLElement) {
  if (root === undefined) {
    root = getRoot();
  }

  root.innerHTML = '';
}

export function addCode(s: string, root?: HTMLElement):void {
  if (root === undefined) {
    root = getRoot();
  }

  const preEl = document.createElement('pre');
  preEl.setAttribute('class', 'language-js');

  const codeEl = document.createElement('code');
  codeEl.setAttribute('class', 'language-js');
  
  var markupHtml = Prism.highlight(s, Prism.languages.javascript);
  codeEl.innerHTML = markupHtml;

  preEl.appendChild(codeEl);
  root.appendChild(preEl);
}

function addNode(
    s: string,
    elName?: string,
    className?:string,
    root?: HTMLElement
  ):void {
  if (elName === undefined) {
    elName = 'div';
  }
  if (root === undefined) {
    root = getRoot();
  }

  const node = document.createElement(elName);
  if (className) {
    node.setAttribute('class', className);
  }
  if (s) {
    node.innerHTML = s;
  }

  root.appendChild(node);
}

export function addH1(s: string, root?: HTMLElement):void {
  addNode(s, 'h1', '', root);
}
export function addH2(s: string, root?: HTMLElement):void {
  addNode(s, 'h2', '', root);
}
export function addH3(s: string, root?: HTMLElement):void {
  addNode(s, 'h3', '', root);
}
export function addDiv(s: string, root?: HTMLElement):void {
  addNode(s, 'div', '', root);
}
export function addHr(root?: HTMLElement):void {
  addNode('', 'hr', '', root);
}

export function addRemark(s: string, root?: HTMLElement):void {
  addNode(s, 'div', 'remark', root);
}
