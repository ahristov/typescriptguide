"use strict";
exports.__esModule = true;
var Prism = require("prismjs");
function getRoot() {
    return document.getElementById('root');
}
exports.getRoot = getRoot;
function clear(root) {
    if (root === undefined) {
        root = getRoot();
    }
    root.innerHTML = '';
}
exports.clear = clear;
function addCode(s, root) {
    if (root === undefined) {
        root = getRoot();
    }
    var preEl = document.createElement('pre');
    preEl.setAttribute('class', 'language-js');
    var codeEl = document.createElement('code');
    codeEl.setAttribute('class', 'language-js');
    var markupHtml = Prism.highlight(s, Prism.languages.javascript);
    codeEl.innerHTML = markupHtml;
    preEl.appendChild(codeEl);
    root.appendChild(preEl);
}
exports.addCode = addCode;
function addNode(s, elName, className, root) {
    if (elName === undefined) {
        elName = 'div';
    }
    if (root === undefined) {
        root = getRoot();
    }
    var node = document.createElement(elName);
    if (className) {
        node.setAttribute('class', className);
    }
    if (s) {
        node.innerHTML = s;
    }
    root.appendChild(node);
}
function addH1(s, root) {
    addNode(s, 'h1', '', root);
}
exports.addH1 = addH1;
function addH2(s, root) {
    addNode(s, 'h2', '', root);
}
exports.addH2 = addH2;
function addH3(s, root) {
    addNode(s, 'h3', '', root);
}
exports.addH3 = addH3;
function addDiv(s, root) {
    addNode(s, 'div', '', root);
}
exports.addDiv = addDiv;
function addHr(root) {
    addNode('', 'hr', '', root);
}
exports.addHr = addHr;
function addRemark(s, root) {
    addNode(s, 'div', 'remark', root);
}
exports.addRemark = addRemark;
